﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour
{
    public static GameControl ins;
    public Text scoreText;

    public GameObject gameOverText;

    private int score = 0;
    public bool gameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        if (ins == null)
            ins = this;
        else if (ins != this)
            Destroy(ins);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameOver)
            if (Input.GetKey(KeyCode.Space))
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StarScored()
    {
        score++;
        scoreText.text = "SCORE: " + score.ToString();

    }


    public void PlayerDied()
    {
        gameOver = true;
        gameOverText.SetActive(true);
    }
}
