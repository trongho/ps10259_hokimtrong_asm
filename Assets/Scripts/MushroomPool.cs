﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomPool : MonoBehaviour
{
    public GameObject mushroomPrefab;
    public int mushroomPoolSize = 5;
    public float spawnedRate = 3f;

    private GameObject[] mushrooms;
    private Vector2 mushroomPoolPosition = new Vector2(-15f, -15f);
    private float mushroomXPosition = 9;
    private float mushroomYPosition;

    private int currentMushroomIndex = 0;
    private float timeSinceLastSpawned = 0;

    private float minY = -3f;
    private float maxY = 3f;

    // Start is called before the first frame update
    void Start()
    {
       mushrooms = new GameObject[mushroomPoolSize];

        for (int i = 0; i < mushroomPoolSize; i++)
            mushrooms[i] = Instantiate(mushroomPrefab, mushroomPoolPosition, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameControl.ins.gameOver)
            return;

        timeSinceLastSpawned += Time.deltaTime;

        if (timeSinceLastSpawned >= spawnedRate)
        {
            timeSinceLastSpawned = 0;
            mushroomXPosition = Random.Range(minY, maxY);
            mushrooms[currentMushroomIndex].transform.position = new Vector2(mushroomXPosition, mushroomYPosition);
            currentMushroomIndex++;

            if (currentMushroomIndex > mushroomPoolSize)
                currentMushroomIndex = 0;
        }
    }
}
