﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    public float leftForce = -200f;
    public float rightForce = 200f;

    private Vector2 velocity;

    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb2d.velocity = Vector2.zero;
        rb2d.AddForce(new Vector2(rightForce, 0));
        rb2d.AddForce(new Vector2(leftForce, 0));
    }
}
