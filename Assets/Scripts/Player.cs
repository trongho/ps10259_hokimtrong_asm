﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 100f;
    public float jumpForce = 400f;
    public float leftForce = -200f;
    public float rightForce = 200f;

    private Vector2 velocity;

    public float jumpHeight = 4f;

    private bool m_FacingRight = true;  // For determining which way the player is currently facing.

    private Rigidbody2D rb2d;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Awake()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        float moveInput = Input.GetAxisRaw("Horizontal");

        if (Input.GetKey(KeyCode.D))
        {
            anim.SetTrigger("isRunning");
            rb2d.velocity = Vector2.zero;
            rb2d.AddForce(new Vector2(rightForce, 0));
        }

        if (Input.GetKey(KeyCode.A))
        {
            anim.SetTrigger("isRunning");
            rb2d.velocity = Vector2.zero;
            rb2d.AddForce(new Vector2(leftForce,0));
        }
        if (Input.GetKey(KeyCode.Space))
        {
            anim.SetTrigger("isJumping");
            rb2d.velocity = Vector2.zero;
            velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
            rb2d.AddForce(new Vector2(0,jumpForce));
        }

        if (moveInput > 0 && !m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (moveInput < 0 && m_FacingRight)
        {
            // ... flip the player.
            Flip();
        }

    }

    private void FixedUpdate()
    {
        
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        transform.Translate(rb2d.velocity * Time.deltaTime);
    }
        
}
